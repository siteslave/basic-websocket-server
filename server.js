const WebSocket = require('ws');
// client socket
websockets = {};

isAlive = false;

const wss = new WebSocket.Server({
  port: 8080,
  // handshakeTimeout: 300000,
  perMessageDeflate: {
    zlibDeflateOptions: { // See zlib defaults.
      chunkSize: 1024,
      memLevel: 7,
      level: 3,
    },
    zlibInflateOptions: {
      chunkSize: 10 * 1024
    },
    // Other options settable:
    clientNoContextTakeover: true, // Defaults to negotiated value.
    serverNoContextTakeover: true, // Defaults to negotiated value.
    clientMaxWindowBits: 10,       // Defaults to negotiated value.
    serverMaxWindowBits: 10,       // Defaults to negotiated value.
    // Below options specified as default values.
    concurrencyLimit: 10,          // Limits zlib concurrency for perf.
    threshold: 1024,               // Size (in bytes) below which messages
    // should not be compressed.
  }
});

wss.on('connection', function connection(ws, req) {
  // const pathname = url.parse(request.url).pathname;
  // console.log(req.url);

  var params = req.url.split('?');
  // console.log(params);
  var roomId = params[1];

  console.log(roomId);

  websockets[roomId] = ws;

  ws.on('message', function incoming(data) {
    wss.clients.forEach(function each(client) {
      if (client !== ws && client.readyState === WebSocket.OPEN) {
        client.send(data);
      }
    });

    // websockets[roomId].send(data);

  });

  ws.on('open', function open() {
    console.log(userId);
    console.log('connected');
    ws.send(Date.now());
  });

  ws.on('close', function close() {
    delete websockets[userId];
    console.log('disconnected');
  });
});