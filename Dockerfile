FROM keymetrics/pm2

LABEL maintainer="Satit Rianpit <rianpit@gmail.com>"

WORKDIR /home/websocket

COPY ./server.js .
COPY ./package.json .
COPY ./process.json .

RUN npm i

CMD [ "pm2-runtime", "start", "process.json" ]

EXPOSE 8080